/*
  GPS Finder
  Copyright (C) 2013 Monami-ya LLC, Japan.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monami_ya.android.gpsfinder;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.android.gms.maps.model.Marker;

public class MarkerStore {
	private static MarkerStore self = null;
	private Map<String, Integer> life = new HashMap<String, Integer>();
	private Map<String, Marker> markers = new HashMap<String, Marker>();

	public static MarkerStore getInstance() {
		if (self == null) {
			self = new MarkerStore();
		}
		return self;
	}

	public void gcMark() {
		synchronized(this) {
			Set<String> set = life.keySet();
			for (String s : set) {
				int i = life.get(s);
				life.put(s, --i);
			}
		}
	}
	
	public void gcSweep() {
		synchronized(this) {
			Set<String> set = life.keySet();
			String[] idArray = (String[]) set.toArray(new String[set.size()]);
			for (String id : idArray) {
				int i = life.get(id);
				if (i <= 0) {
					life.remove(id);
					Marker marker = markers.get(id);
					marker.remove();
					markers.remove(id);
				}
			}
		}
	}

	public boolean exist(String id) {
		synchronized(this) {
			return (markers.get(id) != null);
		}
	}
	
	public void set(String id, Marker marker) {
		synchronized(this) {
			life.put(id, 5);
			markers.put(id, marker);
		}
	}
	
	public Marker get(String id) {
		synchronized(this) {
			life.put(id, 5);
			return markers.get(id);
		}
	}
}
