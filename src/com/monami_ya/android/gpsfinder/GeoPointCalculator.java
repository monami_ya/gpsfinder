/*
 * This code is based on description on http://pugetworks.com/blog/2011/02/gpsview/
 * It uses "Aviation Formulary V1.46" http://williams.best.vwh.net/avform.htm#LL
 */

package com.monami_ya.android.gpsfinder;

import com.google.android.gms.maps.model.LatLng;

public class GeoPointCalculator {
    public static final double EARTH_CIRCUMFRENCE = 40075;
    public static final double EARTH_RADIUS = 6360;
    public static final double GPS_SAT_ORBIT = 20200;
    public static final double SAT_HEIGHT_FROM_CENTER = EARTH_RADIUS + GPS_SAT_ORBIT;
 
 
    public static double findDistance(float elevation) {
    	//These equations are based on the law of sins 
    	if(elevation == 90) return 0;
 
    	double theta = Math.toRadians(elevation) + Math.PI/2; //Add on for the 90 degree tangent
    	double alpha = Math.asin(EARTH_RADIUS * (Math.sin(theta)) / SAT_HEIGHT_FROM_CENTER);
    	double beta = Math.PI - theta - alpha;
    	double distance = beta * EARTH_RADIUS; 
 
    	return distance;
    }
 
    public static LatLng findPosition(double distance, double lat1, double lon1, double azimuth) {
 
    	//Switch to radians
    	lat1 = Math.toRadians(lat1);
    	lon1 = Math.toRadians(lon1);
    	azimuth = Math.toRadians(azimuth);
    	distance = (distance/EARTH_CIRCUMFRENCE)* 2* Math.PI;
 
       	double lat2;
    	double lon2;
 
    	lat2 = Math.asin(Math.sin(lat1)*Math.cos(distance) + Math.cos(lat1) * Math.sin(distance) * Math.cos(azimuth));
    	if(Math.cos(lat1) == 0) {
    		lon2 = lon1; // endpoint a pole
    	} else {
    		lon2 = Math.asin(Math.sin(azimuth)*Math.sin(distance)/Math.cos(lat1));
    		lon2 = lon1 - lon2 + Math.PI;
   			lon2 = (lon2 % (2*Math.PI));   
    	    lon2 = lon2 - Math.PI;
    	}
 
    	//Convert back to degrees
    	lat2 = Math.toDegrees(lat2);
    	lon2 = Math.toDegrees(lon2);
 
    	LatLng pt = new LatLng(lat2, lon2);
    	return pt;
    }
}
