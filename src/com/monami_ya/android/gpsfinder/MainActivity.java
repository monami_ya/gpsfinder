/*
  GPS Finder
  Copyright (C) 2013 Monami-ya LLC, Japan.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monami_ya.android.gpsfinder;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity {

	private GoogleMap map;
	private LocationManager locationManager;
	private LocationListener locationListener = new LocationListener() {
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onLocationChanged(Location location) {
		}
	};

	private final GpsStatus.NmeaListener nmeaListener = new GpsStatus.NmeaListener() {
		@Override
		public void onNmeaReceived(long timestamp, String nmea) {
			String[] d = nmea.split("\\*");
			String[] data = d[0].split(",");

			if (data[0].equals("$GPGSV")) {
				try {
					parseGPGSV(data);
				} catch (ArrayIndexOutOfBoundsException e) {
					Log.d("NMEAListener", "NMEA Error", e);
					Toast.makeText(MainActivity.this, "NMEA Error",
							Toast.LENGTH_LONG).show();
				}
			}
		}

		private void parseGPGSV(String[] data) {
			Location myPosition = map.getMyLocation();
			if (myPosition == null) {
				Toast.makeText(MainActivity.this,
						R.string.detecting_my_location, Toast.LENGTH_SHORT)
						.show();
				return;
			}

			if (data.length >= 4) {
				Paint paint = new Paint();
				paint.setTextSize(30);
				paint.setColor(Color.BLACK);
				for (int index = 4; index < data.length; index += 4) {
					if (data[index].length() == 0
							|| data[index + 1].length() == 0
							|| data[index + 2].length() == 0) {
						continue;
					}

					String id = data[index];
					float elevation = Float.parseFloat(data[index + 1]);
					double distance = GeoPointCalculator
							.findDistance(elevation);
					double azimuth = Double.parseDouble(data[index + 2]);
					LatLng gpsPos = GeoPointCalculator.findPosition(distance,
							myPosition.getLatitude(),
							myPosition.getLongitude(), azimuth);
					String snr;
					try {
						snr = Integer.parseInt(data[index + 3], 10) + "[db]";
					} catch (NumberFormatException e) {
						snr = "unknown";
					}

					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inMutable = true;
					Bitmap bitmap = BitmapFactory.decodeResource(
							getResources(), R.drawable.satellite, options);
					Canvas canvas = new Canvas(bitmap);
					canvas.drawText(id, 0, bitmap.getHeight(), paint);
					canvas.save();
					BitmapDescriptor icon = BitmapDescriptorFactory
							.fromBitmap(bitmap);
					MarkerOptions marker1 = new MarkerOptions();
					marker1.position(gpsPos)
							.icon(icon)
							.title(id)
							.snippet(
									"ELV:" + elevation + "[deg]\nAZI:"
											+ azimuth + "[deg]\nSNR: " + snr);

					if (markerStore.exist(id)) {
						Marker marker = markerStore.get(id);
						marker.setPosition(gpsPos);
						marker.setSnippet(marker.getSnippet());
					} else {
						Marker marker = map.addMarker(marker1);
						markerStore.set(id, marker);
					}
				}
			}
		}
	};

	private final MarkerStore markerStore = new MarkerStore();

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.main);

		map = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		map.setMyLocationEnabled(true);
		map.setTrafficEnabled(false);

		map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker marker) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						MainActivity.this);

				builder.setMessage(marker.getTitle()).create().show();
			}
		});

		map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
			@Override
			public void onMyLocationChange(Location loc) {

				map.moveCamera(CameraUpdateFactory
						.newCameraPosition(new CameraPosition.Builder()
								.target(new LatLng(loc.getLatitude(), loc
										.getLongitude())).zoom(1f).build()));

				map.setOnMyLocationChangeListener(null);
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		LocationProvider p = locationManager
				.getProvider(LocationManager.GPS_PROVIDER);
		String name = p.getName();
		locationManager.addNmeaListener(nmeaListener);
		locationManager.requestLocationUpdates(name, 0, 0, locationListener);

		new Handler().postDelayed(new GCTask(), 1000);
	}

	class GCTask implements Runnable {
		@Override
		public void run() {
			markerStore.gcMark();
			markerStore.gcSweep();
			new Handler().postDelayed(this, 1000);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		locationManager.removeUpdates(locationListener);
		locationManager.removeNmeaListener(nmeaListener);
	}

}